const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      allureWriter(on, config);  //AQUI COLOCAR PARA CCHAMAR O ALLURE ESSA LINHA
    },
    "chromeWebSecurity": false,
    "baseUrl": "https://phptravels.net/", 
    "viewportWidth": 1920,
    "viewportHeight": 1080,
    "screenshotOnRunFailure": true
  },
});
