/// <reference types="cypress" />

import PagamentoPage from "../pages/PagamentoPage";
import SelecaoVooPage from "../pages/SelecaoVooPage";
import SuaViagemPage from "../pages/SuaViagemPage";

describe('Fazer marcação de viagem', () => {
//   beforeEach(() => {
     //cy.visit('https://phptravels.net/')
   //})

  it('Cadastrar uma viagem', () => {
       
    SuaViagemPage.MarcarViagem({Data: "01-05-2024"});
    SelecaoVooPage.SelecionaVoo();
    PagamentoPage.DadosPagamento({Nome: "Kleber", Sobrenome: "Rocha", Email: "kleberocha@gmail.com", Telefone: "123456789", Endereco: "Rua Edson Ramalho"});
    PagamentoPage.DadosPassageiro({Nome: "Kleber", Sobrenome: "Rocha", Nacionalidade: "Brazil", MesNascimento: "06 Jun", DiaNascimento: "13", AnoNascimento: "1982", Passaporte: "123456789101112"})

    // AlugarCarro.SelecaoAeroporto();

    //npx allure serve allure-results
  }); 

});
