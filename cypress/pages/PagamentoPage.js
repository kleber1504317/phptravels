class PagamentoPage {

    DadosPagamento(obj) {

        Cypress.on('uncaught:exception', () => { return false })

        //Nome
        if (obj.Nome != undefined)
            cy.get('[name="user[first_name]"]').type(obj.Nome).tab()

        //Sobrenome
        if (obj.Sobrenome != undefined)
            cy.get('[name="user[last_name]"]').type(obj.Sobrenome).tab()

        //E-mail
        if (obj.Email != undefined)
            cy.get('[name="user[email]"]').type(obj.Email).tab()

        //Telefone
        if (obj.Telefone != undefined)
            cy.get('[name="user[phone]"]').type(obj.Telefone).tab()

        //Endereço
        if (obj.Endereco != undefined)
            cy.get('[name="user[address]"]').type(obj.Endereco).tab()

    }

    DadosPassageiro(obj) {

        //Nome do Passageiro
        if (obj.Nome != undefined)
            cy.get('[name="first_name_1"]').type(obj.Nome).tab()

        //SobreNome do Passageiro
        if (obj.Sobrenome != undefined)
            cy.get('[name="last_name_1"]').type(obj.Sobrenome).tab()

        //Nacionalidade
        if (obj.Nacionalidade != undefined)
            cy.get('[name="nationality_1"]').select(obj.Nacionalidade).tab()

        //Mes nasc
        if (obj.MesNascimento != undefined)
            cy.get('[name="dob_month_1"]').select(obj.MesNascimento).tab()

        //Dia Nasc
        if (obj.DiaNascimento != undefined)
            cy.get('[name="dob_day_1"]').select(obj.DiaNascimento).tab()

        //Ano Nasc
        if (obj.AnoNascimento != undefined)
            cy.get('[name="dob_year_1"]').select(obj.AnoNascimento).tab()

        //Passaporte
        if (obj.Passaporte != undefined)
            cy.get('[name="passport_1"]').type(obj.Passaporte).tab()

        //Pagamento Depois
        cy.get('[id="gateway_pay_later"]').click()

        //Marcação de Acordo
        cy.get('[id="agreechb"]').click()

        //Botão de Confirmar
        cy.get('button[type="submit"]').click()
        cy.wait(10000)
    }

}

export default new PagamentoPage();
