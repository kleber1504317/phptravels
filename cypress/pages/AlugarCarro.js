class AlugarCarro{

    SelecaoAeroporto(){
        Cypress.on('uncaught:exception', () => { return false })
        
        cy.visit('https://phptravels.net/')

        cy.get(':nth-child(3) > .nav-link > span').click()
        
        //Aeroporto
        cy.get('#cars-search > div > div > div > div > span > span > span').click()
        cy.get('#select2--results > div > div:nth-child(2)').click()
        
        //  Setar o valor de um elemento, alterando a propriedade do mesmo.
         cy.get('#date').invoke('prop', 'defaultValue', '01-05-2024')
         cy.get('#date').invoke('prop', 'value', '01-05-2024')

        // cy.get('#date').click()
        // cy.get('[class="day "]').select('1')

    }
}

export default new AlugarCarro();