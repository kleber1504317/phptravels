class SuaViagemPage{

    MarcarViagem(obj){
        Cypress.on('uncaught:exception', () => { return false })

        cy.visit('https://phptravels.net/')
        //Seleciona Cidade Partida
        cy.get('#onereturn > div:nth-child(1) > div > div > span > span.selection > span').click()
        cy.get('#select2--results > div > div:nth-child(1)').click()

        //Seleciona Cidade Destino
        cy.get('#onereturn > div:nth-child(2) > div > div > span > span.selection > span').click()
        cy.get('#select2--results > div > div:nth-child(1)').click()
        
        //Abre Lista de Datas
        if (obj.Data != undefined) {
            cy.get('#departure').clear()
            cy.get('#departure').type(obj.Data).tab()
        }

        //Passageiros
        cy.get('#flights-search > div > div:nth-child(5)').click()
        
        
    }
}

export default new SuaViagemPage();